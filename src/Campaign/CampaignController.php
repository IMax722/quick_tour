<?php

namespace App\Campaign;

use Symfony\Component\HttpFoundation\Exception\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use App\Entity\Campaign;
use App\Entity\Members;
use App\Entity\CampaignType;

class CampaignController extends FOSRestController
{
    public function createCampaign()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $campaign = new Campaign();
        $campaign->setAccountsid(5);
        $campaign->setName('sdcsd');
        $campaign->setCampaigntypeid('slsdcsd');
        $campaign->setMessageend('fgfdg');
        
        $entityManager->persist($campaign);
        
        $entityManager->flush();

        return new Response('Saved new campaign with id '.$campaign->getId());
    }
    public function showAction($id)
    {
        $campaign = $this->getDoctrine()
        ->getRepository(Campaign::class)
        ->find($id);

        if (!$campaign) {
            throw $this->createNotFoundException(
            'No product found for id '.$id
        );
        }
        if ($campaign->getMessageend()=='0') {
            return new Response('Campaign in not active');
        }
    

        return new Response('Check out this great product: '.$campaign->getName());
        {
    }
    }
    public function editCampaign($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $campaign = $entityManager->getRepository(Campaign::class)->find($id);
    
        if (!$campaign) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }
        if ($campaign->getMessageend()=='0') {
            return new Response('Campaign in not active');
        }
    
        $campaign->setName('Maxwell');
        $entityManager->flush();
    
        return new Response('New name of campaign is: '.$campaign->getName(), 301);
    }
    public function deleteCampaign($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $campaign = $entityManager->getRepository(Campaign::class)->find($id);
        $entityManager->remove($campaign);
        $entityManager->flush();

        return new Response('campaign in delete');
    }
}
