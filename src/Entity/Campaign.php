<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CampaignRepository")
 * @ORM\Table(name="campaign")
 * @ORM\HasLifecycleCallbacks
 */

class Campaign
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *  @ORM\Column(type="integer",name="accounts_id")
     */
    private $accountsid;

    /**
     *  @ORM\Column(type="string",name="campaign_type_id")
     */
    private $campaigntypeid;

    /**
     *  @ORM\Column(type="string",name="name")
     */
    private $name;

    /**
     *  @ORM\Column(type="string",name="message_end")
     */
    private $messageend;

    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime",name="created_at")
     */
    protected $created;

    /**
     *  @ORM\Column(type="string",length=32,name="UID")
     */
    private $uid;


    public function getId()
    {
        return $this->id;
    }
    public function getAccountsid()
    {
        return $this->accountsid;
    }
    public function getCampaigntypeid()
    {
        return $this->campaigntypeid;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getMessageend()
    {
        return $this->messageend;
    }
    public function getCreatedat()
    {
        return $this->created;
    }
    public function getUid()
    {
        return $this->uid;
    }

    public function setAccountsid($accountsid)
    {
        $this->accountsid = $accountsid;
    }
    public function setCampaigntypeid($campaigntypeid)
    {
        $this->campaigntypeid = $campaigntypeid;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setMessageend($messageend)
    {
        $this->messageend = $messageend;
    }
    
    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function setCreatedat()
    {
        $this->created = new \DateTime("now");
    }
    public function setUid($uid)
    {
        $this->uid = $uid;
    }
}
