<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CampaignTypeRepository")
 * @ORM\Table(name="campaigntype")
 * @ORM\HasLifecycleCallbacks
 */
class CampaignType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *  @ORM\Column(type="string",name="name")
     */
    private $name;

    /**
     *  @ORM\Column(type="boolean",name="status")
     */
    private $status;

    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime",name="created_at")
     */
    protected $created;

    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getStatus()
    {
        return $this->status;
    }
    public function getCreatedat()
    {
        return $this->created;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setStatus($status)
    {
        $this->status = $status;
    }
    
    /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function setCreatedat()
    {
        $this->created = new \DateTime("now");
    }
}
