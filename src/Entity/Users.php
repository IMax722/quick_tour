<?php

namespace App\Entity;

use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\ExclusionPolicy;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use FOS\UserBundle\Model\Group;
use App\Members\MembersController;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MembersRepository")
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks
 */

 class Users implements UserInterface
 {
     /**
      * @ORM\Id
      * @ORM\GeneratedValue
      * @ORM\Column(type="integer")
      */
     private $id;
    
     /**
      * @ORM\Column(type="string", length=100, name="name")
      *  @Assert\Length(
      *      min = 2,
      *      max = 50,
      *      minMessage = "Your first name must be at least {{ limit }} characters long",
      *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
      * )
      */
     private $username;
    
     /**
      *  @ORM\Column(type="string", length=100, name="email",unique=TRUE)
      *  @Assert\Email(
      *     message = "The email '{{ value }}' is not a valid email.",
      *     checkMX = true
      * )
      */
     private $email;
     
     /**
      *  @ORM\Column(type="string",length=15,name="phone")
      *  @Assert\Length(
      *      min = 2,
      *      max = 50,
      *      minMessage = "Your number must be at least {{ limit }} characters long",
      *      maxMessage = "Your number cannot be longer than {{ limit }} characters"
      * )
      */
     private $phone;
    
     /**
      *  @ORM\Column(type="string",length=30,name="password")
      *  @Assert\Length(
      *      min = 2,
      *      max = 50,
      *      minMessage = "Your password  must be at least {{ limit }} characters long",
      *      maxMessage = "Your password cannot be longer than {{ limit }} characters"
      * )
      */
     private $password;

     /**
      *  @ORM\Column(type="string",unique=true,name="UID")
      */
     private $apiKey;

     /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime",name="created_at")
     */
     protected $created;

     public function getId()
     {
         return $this->id;
     }
     public function getUsername()
     {
         return $this->username;
     }
     public function getEmail()
     {
         return $this->email;
     }
     public function getPhone()
     {
         return $this->phone;
     }
     public function getPassword()
     {
         return $this->password;
     }
     public function getUid()
     {
         return $this->apiKey;
     }
     public function getCreatedat()
     {
         return $this->created;
     }
     public function getRoles()
     {
         return array('ROLE_USER');
     }
     public function getSalt()
     {
     }
     public function eraseCredentials()
     {
     }
     
    
     public function setName($username)
     {
         $this->username = $username;
     }
     public function setEmail($email)
     {
         $this->email = $email;
     }
     public function setPhone($phone)
     {
         $this->phone = $phone;
     }
     public function setPassword($password)
     {
         $this->password = $password;
     }
     public function setUid($apiKey)
     {
         $this->apiKey = $apiKey;
     }

     /**
      * Gets triggered only on insert

      * @ORM\PrePersist
      */
     public function setCreatedat()
     {
         $this->created = new \DateTime("now");
     }
 }
