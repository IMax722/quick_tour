<?php

namespace App\Helper;

class Response
{
    public function lostParameter(string $name)
    {
        return $this->failedResponse(400, "Lost parameter #{$name}", "Lost parameter #{$name}", "001", "link to more info");
    }
    public function invalidParameter(string $name)
    {
        return $this->failedResponse(400, "Bad valid parameter #{$name}", "Bad valid parameter #{$name}", "002", "link to more info");
    }
    protected function failedResponse($status, $developerMessage, $userMessage, $errorCode, $moreInfo)
    {
        return ['status' => $status, 'developerMessage' => $developerMessage,];
        if (is_array($name)) {
            $name = implode(', #', $name);
        }
    }
}
